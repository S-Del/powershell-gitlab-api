do {
	$privateToken = Read-Host "Token"
	$projectId = Read-Host "Project ID"
	$filePath = Read-Host "File Path"
	$confirm = Read-Host "OK? (y/n)"
	Clear-Host
} while ($confirm -ne "y")

$postUrl = "https://gitlab.com/api/v4/projects/" + $projectId + "/uploads"
$headers = @{
	"Content-Type" = "multipart/form-data"
	"PRIVATE-TOKEN" = $privateToken
}
$form = @{
	"file" = Get-Item $filePath
}

$resp = Invoke-WebRequest `
-Uri $postUrl `
-Method POST `
-Headers $headers `
-Form $form

$respJson = ($resp | ConvertFrom-Json)
$respJson.url

exit 0
