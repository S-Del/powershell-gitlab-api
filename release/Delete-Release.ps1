do {
	$privateToken = Read-Host "Token"
	$projectId = Read-Host "Project ID"
	$tagName = Read-Host "Tag Name"
	$confirm = Read-Host "OK? (y/n)"
	Clear-Host
} while ($confirm -ne "y")

$delete_url = "https://gitlab.com/api/v4/projects/" + $projectId + "/releases/" + $tagName
$headers = @{
	"PRIVATE-TOKEN" = $privateToken
}

$resp = Invoke-WebRequest `
-Uri $delete_url `
-Method DELETE `
-Headers $headers

$resp | ConvertFrom-Json

exit 0
