do {
	$privateToken = Read-Host "Token"
	$projectId = Read-Host "Project ID"
	$releaseName = Read-Host "Release Name"
	$tagName = Read-Host "Tag Name"
	$description = Read-Host "Release Description"
	$confirm = Read-Host "OK? (y/n)"
	Clear-Host
} while ($confirm -ne "y")

$postUrl = "https://gitlab.com/api/v4/projects/" + $projectId + "/releases"
$headers = @{
	"Content-Type" = "application/json"
	"PRIVATE-TOKEN" = $privateToken
}
$body = @{
	"name" = $releaseName
	"tag_name" = $tagName
	"description" = $description
}

$resp = Invoke-WebRequest `
-Uri $postUrl `
-Method POST `
-Headers $headers `
-Body ($body | ConvertTo-Json)

$resp | ConvertFrom-Json

exit 0