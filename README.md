# GitLab APIを利用するためのWindows PowerShellスクリプト

## 事前準備
このスクリプトを使用する前に、以下の準備が必要です。
- プロジェクトIDの確認
- アクセストークンの作成 (既にあるなら不要)
- タグの作成 (既にあるなら不要)

## リリースの作成
### スクリプトの実行順序
PowerShellにて以下の順序でスクリプトを実行します。
1. release/Create-Release.ps1  
    プロジェクトのタグに紐付いたリリースを作成します
2. upload/Upload-File.ps1 (ファイルをアップロードしないなら不要)  
    プロジェクトにファイルをアップロードし、ファイルURLを取得します。
3. link/Create-Link.ps1 (ファイルをアップロードしないなら不要)  
    プロジェクトにアップロードされたファイルURLとタグと紐づけ、リリースに反映させます。

## リリースの削除
### 実行するスクリプト
PowerShellにて以下のスクリプトを実行します。
- release/Delete-Release.ps1