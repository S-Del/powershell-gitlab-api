do {
	$privateToken = Read-Host "Token"
	$projectId = Read-Host "Project ID"
	$tagName = Read-Host "Tag Name"
	$linkName = Read-Host "Link Name"
	$linkUrl = Read-Host "Link URL"
	$confirm = Read-Host "OK? (y/n)"
	Clear-Host
} while ($confirm -ne "y")

$getUrl = "https://gitlab.com/api/v4/projects/" + $projectId
$headers = @{
	"PRIVATE-TOKEN" = $privateToken
}

$resp = Invoke-WebRequest `
-Uri $getUrl `
-Method GET `
-Headers $headers

$respJson = ($resp | ConvertFrom-Json)
$webUrl = $respJson.web_url

$postUrl = "https://gitlab.com/api/v4/projects/" + $projectId + "/releases/" + $tagName + "/assets/links"
$headers = @{
	"Content-Type" = "application/json"
	"PRIVATE-TOKEN" = $privateToken
}
$body = @{
	"name" = $linkName
	"url" = $webUrl + $linkUrl
}

$resp = Invoke-WebRequest `
-Uri $postUrl `
-Method POST `
-Headers $headers `
-Body ($body | ConvertTo-Json)

$respJson = ($resp | ConvertFrom-Json)
$respJson

exit 0
